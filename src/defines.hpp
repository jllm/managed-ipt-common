/*
 * defines.hpp
 *
 *  Created on: 19 maj 2014
 *      Author: jakub
 */

#pragma once

#define	EXIT_FAILURE	1	/* Failing exit status.  */
#define	EXIT_SUCCESS	0	/* Successful exit status.  */

#define CLIENT_PID_FILE "/var/run/managed-ipt-client.pid"
#define CLIENT_LOG_FILE "/var/log/managed-ipt-client.log"

#define CLIENT_PID_FILE_DIR "/var/run"
#define CLIENT_LOG_FILE_DIR "/var/log"
