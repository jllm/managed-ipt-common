#include "Request.hpp"

#include <vector>
#include <boost/algorithm/string.hpp>

Request::Request()
{
}

std::string Request::toStringNoSign() const
{
    return "A" + getCommandID() + "#"
        + getTable() + "#"
        + getOperation() + "#"
        + getChain() + "#"
        + getOptionsRules();
}

std::string Request::toStringBase64() const
{
	return "A" + getCommandID() + "#"
			+ Base64::encode(getTable()) + "#"
			+ getOperation() + "#"
			+ Base64::encode(getChain()) + "#"
			+ Base64::encode(getOptionsRules()) + "#"
			+ getSign() + "$";
}

std::string Request::getCommandID() const
{
    return commandID;
}

void Request::setCommandID(const std::string& commandID)
{
    this->commandID = commandID;
}

std::string Request::getTable() const
{
    return table;
}

void Request::setTable(const std::string& table)
{
    this->table = table;
}

std::string Request::getOperation() const
{
    return operation;
}

void Request::setOperation(const std::string& operation)
{
    this->operation = operation;
}

std::string Request::getChain() const
{
    return chain;
}

void Request::setChain(const std::string& chain)
{
    this->chain = chain;
}

std::string Request::getOptionsRules() const
{
    return optionsRules;
}

void Request::setOptionsRules(const std::string& optionsRules)
{
    this->optionsRules = optionsRules;
}

Request Request::createRequest(const std::string& text)
{
    std::vector<std::string> splitVec;
    boost::split(splitVec, text, boost::is_any_of("#"));

    if (splitVec.size() != 6)
	{
		throw std::runtime_error("Invalid request");
	}

	Request req;
    req.setCommandID(splitVec.at(0).substr(1));
    req.setTable(Base64::decode(splitVec.at(1)));
    req.setOperation(splitVec.at(2));
    req.setChain(Base64::decode(splitVec.at(3)));
    req.setOptionsRules(Base64::decode(splitVec.at(4)));
    std::string signWithDelimiter = splitVec.at(5);
    req.setSign(signWithDelimiter.substr(0, signWithDelimiter.length() - 1));
	return req;
}
