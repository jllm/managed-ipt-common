#include "Response.hpp"

Response::Response()
{

}

std::string Response::toStringNoSign() const
{
    return "C" + getCommandID() + "#"
        + getStatus() + "#"
        + getOutput();
}

std::string Response::getCommandID() const
{
    return commandID;
}

void Response::setCommandID(const std::string& commandID)
{
    this->commandID = commandID;
}

std::string Response::getStatus() const
{
    return status;
}

void Response::setStatus(const std::string& status)
{
    this->status = status;
}

std::string Response::getOutput() const
{
    return output;
}

void Response::setOutput(const std::string& output)
{
    this->output = output;
}

Response Response::createResponse(const std::string& text)
{
    std::vector<std::string> splitVec;
    boost::split(splitVec, text, boost::is_any_of("#"));

	if (splitVec.size() != 4)
	{
		throw std::runtime_error("Invalid response");
	}

	Response res;
    res.setCommandID(splitVec.at(0).substr(1));
    res.setStatus(splitVec.at(1));
    res.setOutput(splitVec.at(2));
    std::string signWithDelimiter = splitVec.at(3);
    res.setSign(signWithDelimiter.substr(0, signWithDelimiter.length() - 1));

	return res;
}
