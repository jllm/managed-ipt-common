#pragma once

#include <string>
#include <cstring>
#include <cryptopp/rsa.h>
#include <cryptopp/pssr.h>
#include <cryptopp/osrng.h>
#include <cryptopp/base64.h>

#include "Base64.hpp"

class Signable
{
private:
	std::string _sign;

public:
	void setSign(const std::string&);
	std::string getSign() const;
	bool checkSign(const CryptoPP::RSA::PublicKey&) const;
	void sign(const CryptoPP::RSA::PrivateKey&);
	std::string toString() const;
	virtual std::string toStringNoSign() const = 0;
};
