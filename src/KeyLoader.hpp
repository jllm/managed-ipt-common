#ifndef KEYLOADER_HPP_
#define KEYLOADER_HPP_
#include <string>
#include <fstream>
#include <cryptopp/rsa.h>
#include <cryptopp/files.h>
#include <cryptopp/base64.h>

namespace KeyLoader
{
	CryptoPP::RSA::PublicKey readRSAPublicKey(const std::string&);
	CryptoPP::RSA::PrivateKey readRSAPrivateKey(const std::string&);
	bool fileExists(const std::string&);
}



#endif
