#include "Signable.hpp"

std::string Signable::getSign() const
{
    return _sign;
}

void Signable::setSign(const std::string& sign)
{
    _sign = sign;
}

bool Signable::checkSign(const CryptoPP::RSA::PublicKey& pubKey) const
{
	CryptoPP::RSASS<CryptoPP::PSS, CryptoPP::SHA1>::Verifier verifier(pubKey);

	std::string message = toStringNoSign();
	std::string decodedSignature = Base64::decode(getSign());

	bool result = false;
	CryptoPP::StringSource ss(message+decodedSignature, true,
					new CryptoPP::SignatureVerificationFilter(verifier,
							new CryptoPP::ArraySink((byte*) &result,
									sizeof(result)),
							CryptoPP::SignatureVerificationFilter::PUT_RESULT |
							CryptoPP::SignatureVerificationFilter::SIGNATURE_AT_END));

	return result;
}

void Signable::sign(const CryptoPP::RSA::PrivateKey& prvKey)
{
	CryptoPP::AutoSeededRandomPool rng;
	CryptoPP::RSASS<CryptoPP::PSS, CryptoPP::SHA1>::Signer signer(prvKey);

	std::string signStr;

	CryptoPP::StringSource ss(toStringNoSign(), true,
			new CryptoPP::SignerFilter(rng, signer,
					new CryptoPP::Base64Encoder(new CryptoPP::StringSink(signStr), false)));

	setSign(signStr);
}

std::string Signable::toString() const
{
	return toStringNoSign() + "#" + getSign() + "$";
}
