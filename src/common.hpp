/*
 * utils.hpp
 *
 *  Created on: 2 maj 2014
 *      Author: jakub
 */

#pragma once

#include <stdexcept>
#include <exception>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <array>

#include <boost/program_options.hpp>

#include <boost/shared_ptr.hpp>
#include <boost/lambda/lambda.hpp>

/* common::log */

#include <boost/log/core.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/severity_feature.hpp>
#include <boost/log/sources/record_ostream.hpp>

#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/file.hpp>

#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/utility/empty_deleter.hpp>

#include <boost/date_time/posix_time/posix_time_types.hpp>

/* common::log */

enum severity_level
{
	debug,
	info,
	warning,
	error,
	fatal
};

template<typename CharT, typename TraitsT>
inline std::basic_ostream<CharT, TraitsT>& operator<<(
		std::basic_ostream<CharT, TraitsT>& strm, severity_level lvl)
{
	static const char* const str[] =
			{
					"debug",
					"info",
					"warning",
					"error",
					"fatal"
			};
	if (static_cast<std::size_t>(lvl) < (sizeof(str) / sizeof(*str)))
		strm << str[lvl];
	else
		strm << static_cast<int>(lvl);
	return strm;
}

typedef boost::log::sources::severity_logger_mt<severity_level> Logger;

/**
 * Namaspece z pomocnymi funkcjami.
 */
namespace common
{
	/**
	 * Funkcja do wypisywania zawartości zagnieżdżonego wyjątku.
	 *
	 * @param logger - logger.
	 * @param e - wyjątek.
	 * @param level - poziom wyjątku.
	 * @param severity_level - poziom logowania.
	 */
	void print_exception(boost::log::sources::severity_logger_mt<severity_level> logger,
			const std::exception& e, int level = 0, severity_level severity_level = debug);

	namespace program_options
	{

		/**
		 * Check if 'opt1' and 'opt2' are not specified at the same time.
		 * @param vm - variables_map
		 * @param opt1 - first option name
		 * @param opt2 - second option name
		 */
		void conflicting_options(const boost::program_options::variables_map& vm,
				const char* opt1, const char* opt2);

		/**
		 * Check that of 'for_what' is specified, then 'required_option' is specified too.
		 * @param vm - variables_map
		 * @param for_what
		 * @param required_option
		 */
		void option_dependency(const boost::program_options::variables_map& vm,
				const char* for_what, const char* required_option);

	} /* common::program_options */

	namespace log
	{
		/**
		 *
		 */
		void to_terminal();

		/**
		 *
		 * @param fileName
		 */
		void to_file(std::string fileName);

		/**
		 * Ustawia severity_level.
		 * @param severity_level
		 */
		void set_severity_level(const severity_level severity_level);

		/**
		 * Wycisza wszystkie logi.
		 */
		void silent();

		/**
		 *
		 */
		inline void init()
		{
			boost::log::add_common_attributes();
			to_terminal();
		}

	} /* namespace common::log */

	namespace permissions
	{
		const int read = R_OK;
		const int write = W_OK;
		const int exec = X_OK;
		const int exists = F_OK;

		inline bool isRoot()
		{
			return geteuid() == 0;
		}

		/**
		 * @param path - ścieżka do pliku/katalogu
		 * @param mode - prawa dostępu
		 * @return Prawdę jeżeli prawa dostępu są zgodne, fałsz wpp.
		 */
		inline bool check(std::string path, int mode)
		{
			return access(path.c_str(), mode) != -1;
		}

		template<unsigned int N>
		bool check(const std::array<std::string, N> paths, const std::array<int, N>& modes)
		{
			for (unsigned int i; i < N; i++)
			{
				if (!check(paths[i], modes[i]))
				{
					return false;
				}
			}
			return true;
		}

		class no_access_exception: public std::exception
		{

		};
	}

}
