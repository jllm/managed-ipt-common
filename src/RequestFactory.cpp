/*
 * RequestFactory.cpp
 *
 *  Created on: 25 maj 2014
 *      Author: jakub
 */

#include "RequestFactory.hpp"

namespace RequestFactory
{
	std::unique_ptr<Request> createRequest(const uint32_t id, std::array<std::string, 5> str)
	{
		Request * newRequest = new Request();

		newRequest->setCommandID(std::to_string(id));
		newRequest->setTable(str[0]);
		newRequest->setOperation(str[1]);
		newRequest->setChain(str[2]);
		newRequest->setOptionsRules(str[3]);
		newRequest->setSign(str[4]);

		std::unique_ptr<Request> req(newRequest);
		return req;
	}
}
