/*
 * KeyGen.hpp
 *
 *  Created on: 29 maj 2014
 *      Author: jakub
 */

#pragma once

#include <cryptopp/rsa.h>
#include <cryptopp/files.h>
#include <cryptopp/base64.h>
#include <cryptopp/osrng.h>
#include <string>
#include <memory>
#include <utility>

class KeyGen
{
		const int keySize;

	public:
		KeyGen(const int keySize = 3072);

		/**
		 * Generuje parę kluczy RSA.
		 * @return shared pointer na parę kluczy (pub, priv).
		 */
		std::shared_ptr<std::pair<const CryptoPP::RSA::PublicKey, const CryptoPP::RSA::PrivateKey>> generate();

		/**
		 * Zapisuje klucz prywatny w pliku o podanej nazwie <privateKeyFileName>,
		 * a klucz publiczny pod nazwą <privateKeyFileName>.pub
		 * @param privateKeyFileName - nazwa pliku z kluczem prywatnym.
		 * @param keys - shared_ptr na parę (klucz publiczny, klucz prywatny).
		 */
		void saveKeys(const std::string& s,
				std::shared_ptr<std::pair<const CryptoPP::RSA::PublicKey, const CryptoPP::RSA::PrivateKey>>);
		void savePublicKey(const std::string&, const CryptoPP::PublicKey&);
		void savePrivateKey(const std::string&, const CryptoPP::PrivateKey&);

	private:

		void save(const std::string&, const CryptoPP::BufferedTransformation&);
};

