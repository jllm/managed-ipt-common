#ifndef BASE64_HPP_
#define BASE64_HPP_

#include <cryptopp/pssr.h>
#include <cryptopp/osrng.h>
#include <cryptopp/base64.h>

namespace Base64
{
	std::string encode(std::string);
	std::string decode(std::string);
}

#endif
