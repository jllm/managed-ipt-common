/*
 * KeyGen.cpp
 *
 *  Created on: 29 maj 2014
 *      Author: jakub
 */

#include "KeyGen.hpp"

KeyGen::KeyGen(const int keySize) :
		keySize(keySize)
{

}

std::shared_ptr<std::pair<const CryptoPP::RSA::PublicKey, const CryptoPP::RSA::PrivateKey>> KeyGen::generate()
{
	CryptoPP::AutoSeededRandomPool rnd;

	CryptoPP::RSA::PrivateKey rsaPrivate;
	rsaPrivate.GenerateRandomWithKeySize(rnd, this->keySize);

	CryptoPP::RSA::PublicKey rsaPublic(rsaPrivate);

	return std::shared_ptr<std::pair<const CryptoPP::RSA::PublicKey, const CryptoPP::RSA::PrivateKey>>(
			new std::pair<const CryptoPP::RSA::PublicKey, const CryptoPP::RSA::PrivateKey>(rsaPublic, rsaPrivate));
}

void KeyGen::saveKeys(const std::string& privateKeyFileName,
		std::shared_ptr<std::pair<const CryptoPP::RSA::PublicKey, const CryptoPP::RSA::PrivateKey>> keys)
{
	std::string pub = privateKeyFileName + ".pub";
	savePublicKey(pub, keys->first);
	savePrivateKey(privateKeyFileName, keys->second);
}

void KeyGen::savePublicKey(const std::string& filename, const CryptoPP::PublicKey& key)
{
	CryptoPP::ByteQueue queue;
	key.Save(queue);

	save(filename, queue);
}

void KeyGen::savePrivateKey(const std::string& filename, const CryptoPP::PrivateKey& key)
{
	CryptoPP::ByteQueue queue;
	key.Save(queue);
	save(filename, queue);
}

void KeyGen::save(const std::string& filename, const CryptoPP::BufferedTransformation& bt)
{
	CryptoPP::Base64Encoder encoder(nullptr, false);

	bt.CopyTo(encoder);
	encoder.MessageEnd();

	CryptoPP::FileSink file(filename.c_str());

	encoder.CopyTo(file);
	file.MessageEnd();
}
