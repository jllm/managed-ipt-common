#include "Base64.hpp"

std::string Base64::encode(std::string textToEncode)
{
	std::string result;
	CryptoPP::StringSource ss(textToEncode, true,
			new CryptoPP::Base64Encoder(new CryptoPP::StringSink(result), false));

	return result;
}

std::string Base64::decode(std::string textToDecode)
{
	std::string result;
	CryptoPP::StringSource ss(textToDecode, true,
			new CryptoPP::Base64Decoder(new CryptoPP::StringSink(result)));

	return result;
}
