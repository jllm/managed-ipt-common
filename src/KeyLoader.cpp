#include "KeyLoader.hpp"

bool KeyLoader::fileExists(const std::string& fileName)
{
	std::fstream in;
	in.open(fileName.c_str(), std::fstream::in);

	if (in.fail())
		return false;
	else
	{
		in.close();
		return true;
	}
}

CryptoPP::RSA::PublicKey KeyLoader::readRSAPublicKey(const std::string& fileName)
{
	if(!KeyLoader::fileExists(fileName))
		throw std::runtime_error("File with public key doestn't exists");

	CryptoPP::ByteQueue bytes;
	CryptoPP::FileSource file(fileName.c_str(), true, new CryptoPP::Base64Decoder);

	file.TransferTo(bytes);

	CryptoPP::RSA::PublicKey pubKey;
	pubKey.Load(bytes);

	return pubKey;
}

CryptoPP::RSA::PrivateKey KeyLoader::readRSAPrivateKey(const std::string& fileName)
{
	if(!KeyLoader::fileExists(fileName))
		throw std::runtime_error("File with public key doestn't exists");

	CryptoPP::ByteQueue bytes;
	CryptoPP::FileSource file(fileName.c_str(), true, new CryptoPP::Base64Decoder);

	file.TransferTo(bytes);

	CryptoPP::RSA::PrivateKey privKey;
	privKey.Load(bytes);

	return privKey;
}
