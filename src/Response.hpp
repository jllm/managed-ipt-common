#ifndef RESPONSE_HPP_
#define RESPONSE_HPP_

#include <vector>
#include <boost/algorithm/string.hpp>

#include "Signable.hpp"

class Response : public Signable
{
    std::string commandID;
    std::string status;
    std::string output;

public:
    Response();
    std::string toStringNoSign() const;
    std::string getCommandID() const;
    void setCommandID(const std::string&);
    std::string getStatus() const;
    void setStatus(const std::string&);
    std::string getOutput() const;
    void setOutput(const std::string&);

	static Response createResponse(const std::string&);
};

#endif
