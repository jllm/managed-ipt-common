/*
 * RequestFactory.hpp
 *
 *  Created on: 25 maj 2014
 *      Author: jakub
 */

#pragma once

#include <cstdint>
#include <memory>
#include "Request.hpp"

namespace RequestFactory
{
	/**
	 *
	 * @param id - unikalne dla nadawcy id zapytania.
	 * @param str - tablica zawierająca elementy komunikatu.
	 * @return Utworzony obiekt klasy Request.
	 */
	std::unique_ptr<Request> createRequest(const uint32_t id, std::array<std::string, 5> str);
};

