#ifndef Request_HPP_
#define Request_HPP_

#include "Signable.hpp"
#include "Base64.hpp"

class Request : public Signable
{
protected:
    std::string commandID;
    std::string table;
    std::string operation;
    std::string chain;
    std::string optionsRules;

public:
    Request();
    std::string toStringNoSign() const;
    std::string toStringBase64() const;
    std::string getCommandID() const;
    void setCommandID(const std::string&);
    std::string getTable() const;
    void setTable(const std::string&);
    std::string getOperation() const;
    void setOperation(const std::string&);
    std::string getChain() const;
    void setChain(const std::string&);
    std::string getOptionsRules() const;
    void setOptionsRules(const std::string&);

	static Request createRequest(const std::string&);
};

#endif
