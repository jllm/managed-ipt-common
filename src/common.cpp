/*
 * utils.cpp
 *
 *  Created on: 2 maj 2014
 *      Author: jakub
 */

#include "common.hpp"

namespace common
{
	void print_exception(boost::log::sources::severity_logger_mt<severity_level> lg, const std::exception& e,
			int level, severity_level severity_level)
	{
		BOOST_LOG_SEV(lg, severity_level)<< std::string(level, ' ') << "Exception: " << e.what() << ".";
		try
		{
			std::rethrow_if_nested(e);
		}
		catch(const std::exception& e)
		{
			print_exception(lg, e, level+1, severity_level);
		}
		catch(...)
		{}
	}

	namespace program_options
	{

		/**
		 * Check if 'opt1' and 'opt2' are not specified at the same time.
		 * @param vm - variables_map
		 * @param opt1 - first option name
		 * @param opt2 - second option name
		 */
		void conflicting_options(const boost::program_options::variables_map& vm,
				const char* opt1, const char* opt2)
		{
			if (vm.count(opt1) && !vm[opt1].defaulted()
					&& vm.count(opt2) && !vm[opt2].defaulted())
			{
				throw boost::program_options::error(std::string("Conflicting options '")
						+ opt1 + "' and '" + opt2 + "'");
			}
		}

		/**
		 * Check if check that of 'for_what' is specified, then 'required_option' is specified too.
		 * @param vm - variables_map
		 * @param for_what
		 * @param required_option
		 */
		void option_dependency(const boost::program_options::variables_map& vm,
				const char* for_what, const char* required_option)
		{
			if (vm.count(for_what) && !vm[for_what].defaulted())
			{
				if (vm.count(required_option) == 0 || vm[required_option].defaulted())
				{
					throw boost::program_options::error(std::string("Option '") + for_what
							+ "' requires option '" + required_option + "'");
				}
			}

		}

	} /* common::program_options */

	namespace log
	{
		namespace
		{
			severity_level global_severity_lvl = info;
			boost::log::formatter formatter =
					boost::log::expressions::stream
							<< std::hex << std::setw(8) << std::setfill('0')
							<< boost::log::expressions::attr<unsigned int>("LineID")
							<< ": ["
							<< boost::log::expressions::format_date_time<boost::posix_time::ptime>("TimeStamp",
									"%Y-%m-%d %H:%M:%S")
							<< "] : [" << boost::log::expressions::attr<severity_level>("Severity")
							<< "] " << boost::log::expressions::smessage;
		}

		void to_terminal()
		{
			boost::shared_ptr<boost::log::core> core = boost::log::core::get();
			core->remove_all_sinks();

			typedef boost::log::sinks::synchronous_sink<boost::log::sinks::basic_text_ostream_backend<char>> sink_t;
			boost::shared_ptr<sink_t> sinkCout = boost::log::add_console_log(std::cout,
					boost::log::keywords::auto_flush = true);
			boost::shared_ptr<sink_t> sinkCerr = boost::log::add_console_log(std::cerr,
					boost::log::keywords::auto_flush = true);

			sinkCout->set_filter(boost::log::expressions::attr<severity_level>("Severity") < warning);
			sinkCerr->set_filter(boost::log::expressions::attr<severity_level>("Severity") >= warning);

			sinkCout->set_formatter(formatter);
			sinkCerr->set_formatter(formatter);

			core->add_sink(sinkCout);
			core->add_sink(sinkCerr);

			core->set_filter(boost::log::expressions::attr<severity_level>("Severity") >= global_severity_lvl);
		}

		void to_file(std::string fileName)
		{
			boost::shared_ptr<boost::log::core> core = boost::log::core::get();
			core->remove_all_sinks();

			typedef boost::log::sinks::synchronous_sink<boost::log::sinks::text_file_backend> sink_t;
			boost::shared_ptr<sink_t> sink = boost::log::add_file_log(
					boost::log::keywords::file_name = fileName,
					boost::log::keywords::auto_flush = true,
					boost::log::keywords::rotation_size = 10 * 1024 * 1024);
			sink->set_formatter(formatter);

			core->add_sink(sink);
			core->set_filter(boost::log::expressions::attr<severity_level>("Severity") >= global_severity_lvl);
		}

		void set_severity_level(const severity_level level)
		{
			global_severity_lvl = level;
			boost::log::core::get()->set_filter(boost::log::expressions::attr<severity_level>("Severity") >= level);
		}

		void silent()
		{
			global_severity_lvl = error;
			boost::log::core::get()->set_filter(boost::log::expressions::attr<severity_level>("Severity") >= error);
		}

	} /* namespace common::log */

	namespace permissions
	{
		extern inline bool isRoot();

		extern inline bool check(std::string path, int mode);
	}
}
