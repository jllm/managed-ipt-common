CC := g++
SRCDIR := src
TESTDIR := test
BUILDDIR := build
BINDIR := bin
TEST_TARGET := $(BINDIR)/tester
TEST_MAIN := $(TESTDIR)/tester.cpp

SRCEXT := cpp
TEST_SOURCES := $(shell find $(TESTDIR) -type f -name *.$(SRCEXT))
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
CFLAGS := -std=c++11 -g -Wall -Wextra -DBOOST_LOG_DYN_LINK
LIB := -pthread -lboost_unit_test_framework -lboost_program_options -lboost_log -lboost_log_setup -lboost_thread -lboost_system -lcryptopp 

tester: $(TEST_TARGET)

$(TEST_TARGET): $(OBJECTS) $(TEST_MAIN) $(TEST_SOURCES)
	@mkdir -p $(BINDIR)
	$(CC) $(CFLAGS) -I $(SRCDIR) $^ -o $(TEST_TARGET) $(LIB)
	$(TEST_TARGET)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	$(RM) -r $(BUILDDIR) $(BINDIR)

.PHONY: clean
