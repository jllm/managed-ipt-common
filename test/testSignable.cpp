#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE TEST_SIGNABLE
#endif
#include <boost/test/unit_test.hpp>
#include "Signable.hpp"
#include "Request.hpp"
#include "Response.hpp"
#include "KeyGen.hpp"

BOOST_AUTO_TEST_SUITE(SignableSuite)

BOOST_AUTO_TEST_CASE(testCorrectMsg)
{
    Response res = Response::createResponse("Aid#0#output#sign");
	KeyGen keygen;
	auto keyPair = keygen.generate();
	CryptoPP::RSA::PublicKey pubKey = keyPair->first;
	CryptoPP::RSA::PrivateKey prvKey = keyPair->second;

	res.sign(prvKey);

	BOOST_CHECK(res.checkSign(pubKey));
}

BOOST_AUTO_TEST_CASE(testChangedMsg)
{
	Response res = Response::createResponse("Aid#0#output#sign");
	KeyGen keygen;
	auto keyPair = keygen.generate();
	CryptoPP::RSA::PublicKey pubKey = keyPair->first;
	CryptoPP::RSA::PrivateKey prvKey = keyPair->second;

	res.sign(prvKey);

	res.setOutput("outputbad");

	BOOST_CHECK(!res.checkSign(pubKey));
}

BOOST_AUTO_TEST_SUITE_END()


