/*
 * testResponse.cpp
 *
 *  Created on: 2 maj 2014
 *      Author: jakub
 */

#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#   define BOOST_TEST_MODULE TEST2
#endif
#include <boost/test/unit_test.hpp>
#include "Response.hpp"

BOOST_AUTO_TEST_SUITE(ResponseSuite)

BOOST_AUTO_TEST_CASE(testFromString)
{
    std::string commandID = "idid";
    std::string status = "0";
    std::string output = "output";
    std::string sign = "sign";
    std::string in = "C" + commandID + "#" + status + "#" + output + "#" + sign + "$";
    Response res = Response::createResponse(in);

    BOOST_CHECK(res.toString() == in);
    BOOST_CHECK(res.getCommandID() == commandID);
    BOOST_CHECK(res.getStatus() == status);
    BOOST_CHECK(res.getOutput() == output);
    BOOST_CHECK(res.getSign() == sign);
}

BOOST_AUTO_TEST_SUITE_END()

