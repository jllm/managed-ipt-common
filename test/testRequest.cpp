#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#   define BOOST_TEST_MODULE REQUEST_TEST
#endif
#include <boost/test/unit_test.hpp>
#include "Request.hpp"
#include "../src/Base64.hpp"

BOOST_AUTO_TEST_SUITE(RequestSuite)

BOOST_AUTO_TEST_CASE(testFromString)
{
    std::string commandID = "idid";
    std::string table = Base64::encode("table");
    std::string operation = "operation";
    std::string chain = Base64::encode("chain");
    std::string optionsRules = Base64::encode("options-rules");
    std::string sign = "sign";
    std::string in =
        "A" + commandID + "#" +
        table + "#" +
        operation + "#" +
        chain + "#" +
        optionsRules + "#" +
        sign + "$";
    Request req = Request::createRequest(in);

    BOOST_CHECK(req.toStringBase64() == in);
    BOOST_CHECK(req.getCommandID() == commandID);
    BOOST_CHECK(req.getTable() == Base64::decode(table));
    BOOST_CHECK(req.getOperation() == operation);
    BOOST_CHECK(req.getChain() == Base64::decode(chain));
    BOOST_CHECK(req.getOptionsRules() == Base64::decode(optionsRules));
    BOOST_CHECK(req.getSign() == sign);
}

BOOST_AUTO_TEST_SUITE_END()

