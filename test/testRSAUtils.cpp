/*
 * testKeyGen.cpp
 *
 *  Created on: 29 maj 2014
 *      Author: jakub
 */

#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#   define BOOST_TEST_MODULE RSA_UTILS_TEST
#endif
#include <boost/test/unit_test.hpp>
#include "KeyGen.hpp"
#include "KeyLoader.hpp"
#include <cstdio>

#include <iostream>

BOOST_AUTO_TEST_SUITE(RSAKeysSuite)

	BOOST_AUTO_TEST_CASE(testPubGenSaveLoad)
	{
		std::string privateFileName = "testRSAKey";
		std::string publicFileName = privateFileName + ".pub";

		KeyGen keyGen;

		auto keys = keyGen.generate();
		keyGen.saveKeys(privateFileName, keys);

		CryptoPP::RSA::PublicKey pubKey = KeyLoader::readRSAPublicKey(publicFileName);

		BOOST_CHECK_EQUAL(keys->first.GetModulus(), pubKey.GetModulus());
		BOOST_CHECK_EQUAL(keys->first.GetPublicExponent(), pubKey.GetPublicExponent());

		remove(privateFileName.c_str());
		remove(publicFileName.c_str());
	}

	BOOST_AUTO_TEST_SUITE_END()

